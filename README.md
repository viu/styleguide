# VIU Styleguide

A frontend to your components based online styleguide. Perfect to create living design systems and templates. Requires a [Nunjucks](https://mozilla.github.io/nunjucks/templating.html) parser. Works best with [VIU Launchpad](https://www.npmjs.com/package/@viu/launchpad).

## Getting Started

### Prerequisites

Styleguide requires [NodeJS and NPM](https://nodejs.org). We recommend using [NVM](https://github.com/creationix/nvm) to manage node versions.

### Installing

Install VIU Styleguide into your project

```
npm install @viu/styleguide --save-dev
```

And reference the VIU Styleguide on all of the pages you need to document the styleguide.

```
{% extends '../node_modules/@viu/styleguide/dist/layout.html' %}
```

### Using

On each page based on the styleguide layout, you have three blocks available

```
{% block intro %}
    {# Introductory text to this online styleguide goes here (optional) #}
{% endblock %}

{% block content %}
    {# Free form html content to enhance the styleguide here (optional) #}
{% endblock %}

{% block additional %}
    {# Additional content below the left hand navigation #}
{% endblock %}
```

#### Macros
There is macros to help you describe your design system

##### colortab(color)
Will create a single color tab. Supply it with a color object {name, color, description}.

##### colorcollection(colors)
Will create a collection of colortabs. One tab for each color in the array of colors supplied.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/viu/styleguide/tags). 

## Authors

* **Andreas Nebiker** - *Maintainer* - [VIU](https://www.viu.ch)
* **Raphael Ochsenbein** - *Maintainer* - [VIU](https://www.viu.ch)
* The **Team at VIU** - *Contributors* - [VIU](https://www.viu.ch)

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details

## Acknowledgments

* Thank you, dear customers. We use this styleguide in many of our projects. It is fuel for and the result of great things built with our customers. 
* Inspired by many of the phantastic styleguides and design systems out there. Keep on rocking web community!
